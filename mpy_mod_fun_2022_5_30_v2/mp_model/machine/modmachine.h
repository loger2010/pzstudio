#ifndef MICROPY_INCLUDED_HI3861_MODMACHINE_H
#define MICROPY_INCLUDED_HI3861_MODMACHINE_H

#include "py/obj.h"

extern const mp_obj_type_t machine_uart_type;
extern const mp_obj_type_t mp_machine_i2c_type;
extern const mp_obj_type_t machine_pin_type;
extern const mp_obj_type_t machine_pwm_type;
extern const mp_obj_type_t machine_adc_type;

#endif // MICROPY_INCLUDED_ESP32_MODMACHINE_H
