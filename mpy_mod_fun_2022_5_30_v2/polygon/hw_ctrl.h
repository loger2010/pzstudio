#ifndef __HAL_CTRL_H
#define __HAL_CTRL_H

#include "iot_gpio.h"
#include "iot_i2c.h" 
#include "hi_spi.h"
#include "iot_pwm.h"
#include "hi_io.h"
#include "hi_adc.h"
#include "hi_pwm.h"
#include "hi_i2c.h"
#include "hi_gpio.h"
#include "hi_uart.h"


void all_peripheral_deinit();


#endif

