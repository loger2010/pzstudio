#ifndef MY_CRC16_H
#define MY_CRC16_H
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "ohos_types.h"
uint16 my_crc16(uint8 *buf,int len ,int invert);

#endif